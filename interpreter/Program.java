package interpreter;

import java.util.*;

import common.SymbolTable;

public class Program {
    private List<String> input;
    private List<Command> commands;
    private Scanner sc;
    private OutputStream out;
    private int max_memory;
    private int[] memory;
    
    public void run() {
	for (String var : input) {
	    out.print(var + " = ? ");
	    st.setValue(var, sc.nextInt());
	}
	for (Command command : commands) {
	    command.run(st);
	}
    }
    public SymbolTable getTable() {
	return new SymbolTable();
    }
}

