package interpreter.commands;

public abstract class Command {
    private int result;
    private Operand first_operand;

    public int getResult() { return result; }
    public Operand getFirstOperand() { return first_operand; }

    public abstract void run(SymbolTable st);
}

