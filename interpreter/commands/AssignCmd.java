package interpreter.commands;

public class AssignCmd extends Command {
    public void run(SymbolTable st) {
	st.setValue(st.getName(getResult()), st.getValue(st.getName(getFirstOperand())));
    }
}

