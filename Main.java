
import java.io.*;
import java.util.*;
import common.SymbolTable;
import common.Lexeme;
import parser.Parser;
import syntax.Syntax;
import interpreter.Program;
import interpreter.Value;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
	SymbolTable st = new SymbolTable();
	
	Parser parser = new Parser(st, new Scanner(new FileInputStream(args[0])));
	parser.read(); // read from file
	List<Lexeme> lexemes = parser.parse(); // parse and return list of lexemes

	Program program = Syntax.analyze(st, lexemes); // analyze list of lexemes and return program

	program.run(); // run interpreter

	// get results and output them
	SymbolTable result = program.getTable();
	for (String var : result.getVars()) {
	    System.out.println(var + " = " + st.getValue(var));
	}
    }
}

