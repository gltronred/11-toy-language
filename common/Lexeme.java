package common;

// [[:alpha:]]
// RTFM: man perlre

public class Lexeme {
    enum Type { VAR,
		INT,
		ASSIGN,
		SEMICOLON,
		LBRACKET,
		RBRACKET,
		PLUS,
		MULT,
		DOT };
    private Type type;
    private int extra;
}

