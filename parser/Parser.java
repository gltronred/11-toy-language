package parser;

import java.util.*;

import common.SymbolTable;
import common.Lexeme;

public class Parser {
    SymbolTable st;
    Scanner sc;
    
    public Parser(SymbolTable st, Scanner sc) {
	this.st = st;
	this.sc = sc;
    }

    public void read() {
    }

    public List<Lexeme> parse() {
	return new LinkedList<>();
    }
}

